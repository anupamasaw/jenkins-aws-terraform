# Provider - AWS , Specifying Region= North-Virginia(us-east-1)
provider "aws" {
       region = "us-east-1"
       }

# Creating VPC

    resource "aws_vpc" "main" {
       cidr_block       = var.vpc_cidr
       instance_tenancy = var.tenancy

       tags = {
         Name = "my-vpc"
        }
       }

# Creating Internet-Gateway(igw)

    resource "aws_internet_gateway" "main" {
        vpc_id = aws_vpc.main.id
       }

# Printing the ids VPC and IGW

      output "vpc_id" {
        value = "${aws_vpc.main.id}"
        }
       

      output "gateway_id" {
         value = "${aws_internet_gateway.main.id}"
      }


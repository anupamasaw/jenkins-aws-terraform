variable "vpc_cidr" {
     default = "192.168.0.0/16"
         }

 variable "tenancy" {
    default = "default"
         }
 
variable "subnet_cidr_1" {
    default = "192.168.1.0/24"
          }

 variable "subnet_cidr_2" {
    default = "192.168.5.0/24"
          }

 variable "availability_zone_1" {
    default = "us-east-1a"
          }

 variable "availability_zone_2" {
    default = "us-east-1b"
           }

 variable "instance_type" {
    default = "t2.micro"
       }

 variable "ami_id" {
    default = "ami-09d95fab7fff3776c"
     }


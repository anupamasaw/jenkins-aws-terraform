# creating ec2-instance with public-ip and in az = us-east-1a

 resource "aws_instance" "web" {
  ami                         = var.ami_id
  instance_type               = var.instance_type
  subnet_id                   = aws_subnet.subnet_1.id
  key_name                    = aws_key_pair.newkey.key_name
  associate_public_ip_address = true
  user_data = <<-EOF
              #!/bin/bash
              sudo yum update -y
              sudo yum install java
              sudo wget -O /etc/yum.repos.d/jenkins.repo https://pkg.jenkins.io/redhat-stable/jenkins.repo
              sudo rpm --import https://pkg.jenkins.io/redhat-stable/jenkins.io.key
              sudo yum install -y jenkins
              sudo systemctl start jenkins
              sudo systemctl enable jenkins
              EOF


  tags = {
    Name = "Instance-1"
    }
  }



# Associating key-pair

  resource "aws_key_pair" "newkey" {
    public_key = file("./access.pub")

   }

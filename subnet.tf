#Creating Subnet in Availability-zone= us-east-1a

   resource "aws_subnet" "subnet_1" {
       vpc_id            = aws_vpc.main.id
       availability_zone = var.availability_zone_1
       cidr_block        = var.subnet_cidr_1

           tags = {
          Name = "subnet-1"
          }
           }

# Creating subnet in Availability-zone= us-east-1b

  resource "aws_subnet" "subnet_2" {
      vpc_id            = aws_vpc.main.id
      availability_zone = var.availability_zone_2
      cidr_block        = var.subnet_cidr_2

            tags = {
          Name = "subnet-2"
          }
           }

# Printing id of Subnet-1 and Subnet-2

   output "subnet_id_1" {
         value = "${aws_subnet.subnet_1.id}"
      }

           output "subnet_id_2" {
         value = "${aws_subnet.subnet_2.id}"
      }


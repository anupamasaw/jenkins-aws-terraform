# Creating Route-Table for VPC

   resource "aws_route_table" "r" {
        vpc_id = aws_vpc.main.id

    route {
        cidr_block = "0.0.0.0/0"
        gateway_id = aws_internet_gateway.main.id
        }

        tags = {
           Name = "route-table"
           }
     }

# Associating Subnet-1 with Route-Table

   resource "aws_route_table_association" "rt-1" {
        subnet_id      = aws_subnet.subnet_1.id
        route_table_id = aws_route_table.r.id
       }

# Associating Subnet-2 with Route-Table

   resource "aws_route_table_association" "rt-2" {
       subnet_id      = aws_subnet.subnet_2.id
       route_table_id = aws_route_table.r.id
       }

# Printing id of Route-table

    output "route_table_id" {
         value = "${aws_route_table.r.id}"
      }

